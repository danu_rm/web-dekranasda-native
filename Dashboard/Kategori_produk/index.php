<?php
include '../header.php';
include '../../koneksi.php';
$page = "Kategori";
?>

<title>Data Kategori Produk - APPS</title>

<body>
    <div id="app">
        <?php include '../sidebar.php'; ?>
        <div id="main" class='layout-navbar'>
            <?php include '../navbar.php'; ?>
            <div id="main-content">

                <div class="page-heading">
                    <div class="page-title">
                        <div class="row">
                            <div class="col-12 col-md-6 order-md-1 order-last">
                                <h3>Kategori Produk</h3>
                                <p class="text-subtitle text-muted">Kategori/Jenis, setiap produk pasti memiliki kategori</p>
                            </div>
                            <div class="col-12 col-md-6 order-md-2 order-first">
                                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Kategori</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <section class="section">
                        <div class="card">
                            <div class="card-header">
                                <a type="button" data-bs-toggle="modal" data-bs-target="#modalAdd">
                                    <i class="icon dripicons-plus"></i> Add Data
                                </a>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kategori</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <?php

                                            $tampil = mysqli_query($conn, "SELECT * FROM kategori_produk ") or die(mysqli_error($conn));


                                            $no = 0;
                                            while ($data = mysqli_fetch_array($tampil)) //memanggil data secara array
                                            {
                                                $no++;
                                                $id = $data['id_kategori'];
                                            ?>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $data['nama_kategori']; ?></td>

                                                <td>
                                                    <a class="btn btn-light" type="button" data-bs-toggle="modal" data-bs-target="#modalEdit<?= $id ?>"><i class="icon dripicons-document-edit"></i></a>
                                                    <a class="btn btn-light" href="aksi?id=<?= $id; ?>&aksi=hapus" onclick=" javascript: return confirm('Anda Yakin Ingin Menghapusnya?')"><i class="icon dripicons-trash"></i>
                                                    </a>
                                                </td>
                                        </tr>
                                        <!-- MODAL EDIT -->
                                        <div class="modal fade text-left" id="modalEdit<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel33">Edit Data Users </h4>
                                                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                                            <i data-feather="x"></i>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <?php
                                                        $query_edit = mysqli_query($conn, "SELECT * FROM kategori_produk WHERE id_kategori='$id'");
                                                        while ($row = mysqli_fetch_array($query_edit)) {
                                                        ?>
                                                            <form action="aksi?aksi=edit" method="POST" enctype="multipart/form-data">
                                                                <label>Nama Kategori: </label>
                                                                <div class="form-group">
                                                                    <input type="text" name="id_kategori" value="<?= $id ?>" hidden>
                                                                    <input type="text" placeholder="Nama Kategori" name="nama_<?= $id ?>" class="form-control" value="<?= $row['nama_kategori'] ?>">
                                                                </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                                                            <i class="bx bx-x d-block d-sm-none"></i>
                                                            <span class="d-none d-sm-block">Close</span>
                                                        </button>
                                                        <button type="submit" class="btn btn-primary ml-1">
                                                            <i class="bx bx-check d-block d-sm-none"></i>
                                                            <span class="d-none d-sm-block">Edit</span>
                                                        </button>
                                                    </div>
                                                <?php
                                                        }
                                                ?>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                            }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
                <!--Form Modal -->
                <div class="modal fade text-left" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Tambah Data Users </h4>
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                    <i data-feather="x"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="aksi?aksi=tambah" method="POST">
                                    <label>Nama Ketegori: </label>
                                    <div class="form-group">
                                        <input type="text" placeholder="Nama Kategori" name="nama" class="form-control" required>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                                    <i class="bx bx-x d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Close</span>
                                </button>
                                <button type="submit" class="btn btn-primary ml-1">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Tambah</span>
                                </button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

                <?php include '../copyright.php'; ?>

            </div>
        </div>
    </div>
    <?php include 'footer.php'; ?>
</body>

</html>