<?php
include '../../koneksi.php';

$aksi = $_GET['aksi'];
if ($aksi == "tambah") {
    $nik = $_POST['nik'];
    $username = $_POST['username'];
    $password = $_POST['pw'];
    $email = $_POST['email'];
    $nama_users = $_POST['nama'];
    $tempat_lahir = $_POST['tempat_lahir'];
    $tgl_lahir = $_POST['tgl_lahir'];
    $level = $_POST['level'];
    $foto = $_FILES['foto'];
    // var_dump($username);
    // die;
    if ($_FILES['foto']['size'] != 0) { //cek apakah ada foto yg di upload
        $nama = $_FILES['foto']['name'];
        $nama = str_replace(' ', '_', $nama);
        // var_dump($nama);
        // die;
        $ukuran    = $_FILES['foto']['size'];
        $file_tmp = $_FILES['foto']['tmp_name'];

        // $query = mysqli_query($conn, "INSERT INTO upload VALUES(NULL, '$nama')");
        $simpan = mysqli_query($conn, "INSERT INTO users VALUES ('$nik','$username','$password','$email','$nama_users','$tempat_lahir','$tgl_lahir','$level','$foto')") or die(mysqli_error($conn));
        if ($simpan) {
            move_uploaded_file($file_tmp, '../../assets_dash/assets/uploads/users/' . $nama);
            echo "<script>alert('Data Berhasil Disimpan') </script>";
            echo '<script type="text/javascript">window.location="http://localhost/web-dekranasda-native/Dashboard/Users" </script>';
        } else {
            echo "<script>alert('Data Gagal Disimpan') </script>";
            echo "<script>history.go(-1)</script>";
        }
    } else { // jika tidak ada foto yg diupload
        $simpan = mysqli_query($conn, "INSERT INTO users VALUES ('$nik','$username','$password','$email','$nama_users','$tempat_lahir','$tgl_lahir','$level',NULL)") or die(mysqli_error($conn));
        if ($simpan) {
            echo "<script>alert('Data Berhasil Disimpan') </script>";
            echo '<script type="text/javascript">window.location="http://localhost/web-dekranasda-native/Dashboard/Users" </script>';
        } else {
            echo "<script>alert('Data Gagal Disimpan') </script>";
            echo "<script>history.go(-1)</script>";
        }
    }
} elseif ($aksi == "edit") {
    // EDIT
    $i = $_GET['nik_old'];

    $nik = $_POST['nik_' . $i];
    $username = $_POST['username_' . $i];
    $password = $_POST['pw_' . $i];
    $email = $_POST['email_' . $i];
    $nama_users = $_POST['nama_' . $i];
    $tempat_lahir = $_POST['tempat_lahir_' . $i];
    $tgl_lahir = $_POST['tgl_lahir_' . $i];
    $level = $_POST['level_' . $i];
    $foto = $_FILES['foto_' . $i];

    if ($_FILES['foto_' . $i]['size'] != 0) { //cek apakah ada foto yg di upload
        $nama = $_FILES['foto_' . $i]['name'];
        $nama = str_replace(' ', '_', $nama);
        // var_dump($nama);
        // die;
        $ukuran    = $_FILES['foto_' . $i]['size'];
        $file_tmp = $_FILES['foto_' . $i]['tmp_name'];

        // $query = mysqli_query($conn, "INSERT INTO upload VALUES(NULL, '$nama')");
        $simpan = mysqli_query($conn, "UPDATE users SET nik='$nik', username='$username', password='$password', email_users='$email',nama_users='$nama_users',tempat_lahir='$tempat_lahir',tgl_lahir='$tgl_lahir',level='$level',foto='$foto' WHERE nik='$i'") or die(mysqli_error($conn));
        if ($simpan) {
            move_uploaded_file($file_tmp, '../../assets_dash/assets/uploads/users/' . $nama);
            echo "<script>alert('Data Berhasil Disimpan') </script>";
            echo '<script type="text/javascript">window.location="http://localhost/web-dekranasda-native/Dashboard/Users" </script>';
        } else {
            echo "<script>alert('Data Gagal Disimpan') </script>";
            echo "<script>history.go(-1)</script>";
        }
    } else { // jika tidak ada foto yg diupload
        $simpan = mysqli_query($conn, "UPDATE users SET nik='$nik', username='$username', password='$password', email_users='$email',nama_users='$nama_users',tempat_lahir='$tempat_lahir',tgl_lahir='$tgl_lahir',level='$level' WHERE nik='$i'") or die(mysqli_error($conn));
        if ($simpan) {
            echo "<script>alert('Data Berhasil Disimpan') </script>";
            echo '<script type="text/javascript">window.location="http://localhost/web-dekranasda-native/Dashboard/Users" </script>';
        } else {
            echo "<script>alert('Data Gagal Disimpan') </script>";
            echo "<script>history.go(-1)</script>";
        }
    }
    // EDIT
} elseif ($aksi == "hapus") {
    $nik = $_GET['nik'];
    $hapus = mysqli_query($conn, "DELETE FROM users WHERE nik = '$nik'") or die(mysqli_error($conn));
    // var_dump($hapus);
    // die;
    if ($hapus) {
        echo "<script>alert('Data Berhasil Dihapus') </script>";
        echo '<script type="text/javascript">window.location="http://localhost/web-dekranasda-native/Dashboard/Users" </script>';
    } else {
        echo "<script>alert('Data Gagal Dihapus') </script>";
        echo "<script>history.go(-1)</script>";
    }
}
