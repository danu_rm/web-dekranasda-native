<?php
include '../header.php';
include '../../koneksi.php';
$page = "Users";
?>
<title>Data Users - APPS</title>

<body>
    <div id="app">
        <?php include '../sidebar.php'; ?>
        <div id="main" class='layout-navbar'>
            <?php include '../navbar.php'; ?>
            <div id="main-content">

                <div class="page-heading">
                    <div class="page-title">
                        <div class="row">
                            <div class="col-12 col-md-6 order-md-1 order-last">
                                <h3>Data Users</h3>
                                <p class="text-subtitle text-muted">Data Users, Hanya diakses oleh admin</p>
                            </div>
                            <div class="col-12 col-md-6 order-md-2 order-first">
                                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Users</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <section class="section">
                        <div class="card">
                            <div class="card-header">
                                <a type="button" data-bs-toggle="modal" data-bs-target="#inlineForm">
                                    <i class="icon dripicons-plus"></i> Add Data
                                </a>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIK</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Tempat, Tanggal Lahir</th>
                                            <th>Foto</th>
                                            <th>Hak Akses</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <?php

                                            $tampil = mysqli_query($conn, "SELECT * FROM users ORDER BY nik") or die(mysqli_error($conn));


                                            $no = 0;
                                            while ($data = mysqli_fetch_array($tampil)) //memanggil data secara array
                                            {
                                                $no++;
                                                $nik = $data['nik'];
                                            ?>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $nik; ?></td>
                                                <td><?php echo $data['nama_users']; ?></td>
                                                <td><?php echo $data['email_users']; ?></td>
                                                <td><?php echo $data['tempat_lahir'] . ", " . $data['tgl_lahir']; ?></td>
                                                <?php if ($data['foto_users']) { ?>
                                                    <td><img src="http://localhost/web-dekranasda-native/assets_dash/assets/uploads/users/<?= $data['foto_users'] ?>" alt="foto" width="35"></td>
                                                <?php } else {
                                                    echo "<td><b>No Image</b></td>";
                                                } ?>
                                                <td><?php echo $data['level']; ?></td>

                                                <td>
                                                    <a class="btn btn-light" type="button" data-bs-toggle="modal" data-bs-target="#inlineFormEdit<?= $data['nik'] ?>"><i class="icon dripicons-document-edit"></i></a>
                                                    <a class="btn btn-light" href="aksi?nik=<?= $nik; ?>&aksi=hapus" onclick=" javascript: return confirm('Anda Yakin Ingin Menghapusnya?')"><i class="icon dripicons-trash"></i>
                                                    </a>
                                                </td>
                                        </tr>
                                        <!-- MODAL EDIT -->
                                        <div class="modal fade text-left" id="inlineFormEdit<?= $data['nik'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel33">Edit Data Users </h4>
                                                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                                            <i data-feather="x"></i>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <?php
                                                        $id = $data['nik'];
                                                        $query_edit = mysqli_query($conn, "SELECT * FROM users WHERE nik='$id'");
                                                        while ($row = mysqli_fetch_array($query_edit)) {
                                                        ?>
                                                            <form action="aksi?aksi=edit&nik_old=<?= $id ?>" method="POST" enctype="multipart/form-data">
                                                                <label>NIK: </label>
                                                                <div class="form-group">
                                                                    <input type="number" placeholder="NIK" name="nik_<?= $id ?>" class="form-control" value="<?= $row['nik'] ?>">
                                                                </div>
                                                                <label>Username: </label>
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="Username" name="username_<?= $id ?>" class="form-control" value="<?= $row['username'] ?>">
                                                                </div>
                                                                <label>Password: </label>
                                                                <div class="form-group">
                                                                    <input type="password" placeholder="Password" name="pw_<?= $id ?>" class="form-control" value="<?= $row['password'] ?>">
                                                                </div>
                                                                <label>Nama: </label>
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="Nama Users" name="nama_<?= $id ?>" class="form-control" value="<?= $row['nama_users'] ?>">
                                                                </div>
                                                                <label>Email: </label>
                                                                <div class="form-group">
                                                                    <input type="email" placeholder="Email" name="email_<?= $id ?>" class="form-control" value="<?= $row['email_users'] ?>">
                                                                </div>
                                                                <label>Tempat, Tanggal Lahir: </label>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-lg-6">
                                                                            <input type="text" placeholder="Tempat Lahir" name="tempat_lahir_<?= $id ?>" class="form-control" value="<?= $row['tempat_lahir'] ?>">
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <input type="date" placeholder="Tanggal Lahir" name="tgl_lahir_<?= $id ?>" class="form-control" value="<?= $row['tgl_lahir'] ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <label>Level: </label>
                                                                <div class="form-group">
                                                                    <select name="level_<?= $id ?>" class="form-select">
                                                                        <option value="Petugas">Petugas</option>
                                                                        <option value="Penjual">Penjual</option>
                                                                        <option value="Pembeli">Pembeli</option>
                                                                    </select>
                                                                </div>
                                                                <label>Foto: </label>
                                                                <div class="form-group">
                                                                    <input type="file" name="foto_<?= $id ?>" accept="image/x-png,image/gif,image/jpeg" class="form-control">
                                                                </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                                                            <i class="bx bx-x d-block d-sm-none"></i>
                                                            <span class="d-none d-sm-block">Close</span>
                                                        </button>
                                                        <button type="submit" class="btn btn-primary ml-1">
                                                            <i class="bx bx-check d-block d-sm-none"></i>
                                                            <span class="d-none d-sm-block">Edit</span>
                                                        </button>
                                                    </div>
                                                <?php
                                                        }
                                                ?>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                            }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
                <!--Form Modal -->
                <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Tambah Data Users </h4>
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                    <i data-feather="x"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="aksi?aksi=tambah" method="POST" enctype="multipart/form-data">
                                    <label>NIK: </label>
                                    <div class="form-group">
                                        <input type="number" placeholder="NIK" name="nik" id="nik" class="form-control">
                                    </div>
                                    <label>Username: </label>
                                    <div class="form-group">
                                        <input type="text" placeholder="Username" name="username" id="username" class="form-control">
                                    </div>
                                    <label>Password: </label>
                                    <div class="form-group">
                                        <input type="password" placeholder="Password" name="pw" id="pw" class="form-control">
                                    </div>
                                    <label>Nama: </label>
                                    <div class="form-group">
                                        <input type="text" placeholder="Nama Users" name="nama" id="nama" class="form-control">
                                    </div>
                                    <label>Email: </label>
                                    <div class="form-group">
                                        <input type="email" placeholder="Email" name="email" id="email" class="form-control">
                                    </div>
                                    <label>Tempat, Tanggal Lahir: </label>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <input type="text" placeholder="Tempat Lahir" name="tempat_lahir" id="tempat_lahir" class="form-control">
                                            </div>
                                            <div class="col-lg-6">
                                                <input type="date" placeholder="Tanggal Lahir" name="tgl_lahir" id="tgl_lahir" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Level: </label>
                                    <div class="form-group">
                                        <select name="level" id="level" class="form-select">
                                            <option value="Petugas">Petugas</option>
                                            <option value="Penjual">Penjual</option>
                                            <option value="Pembeli">Pembeli</option>
                                        </select>
                                    </div>
                                    <label>Foto: </label>
                                    <div class="form-group">
                                        <input type="file" name="foto" id="foto" accept="image/x-png,image/gif,image/jpeg" class="form-control">
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                                    <i class="bx bx-x d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Close</span>
                                </button>
                                <button type="submit" class="btn btn-primary ml-1">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Tambah</span>
                                </button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

                <?php include '../copyright.php'; ?>

            </div>
        </div>
    </div>
    <?php include 'footer.php'; ?>
</body>

</html>