<footer>
    <div class="footer clearfix mb-0 text-muted">
        <div class="float-start">
            <p>2021 &copy; Dinas Tenaga Kerja dan Perindustrian</p>
        </div>
        <div class="float-end">
            <p>Crafted with <span class="text-danger"><i class="bi bi-heart"></i></span> by <a href="http://wa.me/6282254904247">Danu</a></p>
        </div>
    </div>
</footer>