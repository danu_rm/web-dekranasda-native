<!-- Footer -->
<footer class="py-2 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Dinas Tenaga Kerja dan Perindustrian Kab. Tala</p>
    </div>
    <!-- /.container -->
</footer>