<?php
include 'header.php';
include 'nav.php';
include '../koneksi.php';
?>

<body>
  <!-- Carousel -->
  <div class="container pt-3 d-none d-lg-block">
    <div class="row">
      <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel" style="border-radius: 2px;">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner" data-bs-interval="4000">
          <?php

          $banner = mysqli_query($conn, "SELECT * FROM banner ORDER BY id_banner") or die(mysqli_error($conn));
          $no = 0;
          $active = 0;
          while ($data = mysqli_fetch_array($banner)) //memanggil data secara array
          {
            if ($active == 0) {
              $act = "active";
            } else {
              $act = "";
            }
            $active++;
            $no++;
            $id = $data['id_banner'];
          ?>
            <div class="carousel-item <?= $act ?>">
              <img class="d-block img-fluid rounded car" src="../assets/upload/banner/<?= $data['nama_file']; ?>" alt="<?= $data['nama_file']; ?>">
            </div>
          <?php } ?>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
    </div>
  </div>
  <!-- Carousel End -->

  <!-- KAtegori -->
  <section id="kategori">
    <div class="container mt-3">
      <div class="card p-3 pb-1 rounded" style="border: 0;">
        <div class="row justify-content-center">
          <?php for ($i = 0; $i < 8; $i++) { ?>

            <div class="col-lg-2 col-md-3 col-3 mb-3 text-center">
              <div>
                <img src="http://placehold.it/80x80" class="img-thumbnail" alt="..."><br>
              </div>
              <span>Makanan Ringan</span>
            </div>
          <?php } ?>

        </div>
      </div>
    </div>
  </section>
  <!-- KAtegori -->

  <div class="container p-3">
    <div class="row">
      <?php for ($i = 0; $i < 12; $i++) { ?>
        <div class="col-lg-2 col-md-3 col-6 p-1 rounded">
          <div class="card" style="border: 0;">
            <a href="#"><img class="card-img-top" src="http://placehold.it/700x700" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Item One</a>
              </h4>
              <h5>$24.99</h5>
              <small class="text-muted">213 Terjual</small>
            </div>
          </div>
        </div>
      <?php } ?>

      <!-- /.row -->
    </div>
  </div>
  <!-- /.container -->
  <?php
  include 'copyright.php';
  include 'footer.php';
  ?>