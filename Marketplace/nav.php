<?php
session_start();
if (!isset($cari)) {
	$cari = NULL;
} ?>
<!-- Header info -->
<div class="bg d-none d-sm-block p-2" style="background-color: #e2edff;">
	<div class="container text-white">
		<small style="color: #dbdbdb;">
			<a href="https://wa.me/6282254904247" class="link">Contact Centre</a> |
			<a href="https://wa.me/6282254904247" class="link">Mulai Jual</a> |
			<?php
			if (!isset($_SESSION['login'])) { ?>
				<a href="../login.php" class="link">Login</a>
			<?php } else { ?>
				<a href="../logout.php" class="link">Logout</a>
			<?php } ?>
		</small>
		<small style="color: #dbdbdb; float:right; ">
			<a href="https://wa.me/6282254904247" class="icon"><i style="font-size: 1.5em; color: #3f729b;" class="fab fa-instagram"></i></a>
			<a href="https://wa.me/6282254904247" class="icon"><i style="font-size: 1.5em; color: #2bff00;" class="fab fa-whatsapp"></i></a>
			<a href="https://wa.me/6282254904247" class="icon"><i style="font-size: 1.5em; color: #3b5998;" class="fab fa-facebook"></i></a>
			<a href="https://wa.me/6282254904247" class="icon"><i style="font-size: 1.5em; color: #c4302b;" class="fab fa-youtube"></i></a>
		</small>
	</div>
</div>
<!-- Header infor -->
<div class="d-lg-none d-md-block container p-2 pb-0">
	<a class="btn btn-primary" data-bs-toggle="offcanvas" href="#menu" role="button" aria-controls="menu">
		<i class="fa fa-bars"> </i>
	</a>
</div>
<div class="offcanvas offcanvas-start" tabindex="-1" id="menu" aria-labelledby="menuLabel">
	<div class="offcanvas-header">
		<h3 class="offcanvas-title">Menu</h3>
		<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
	</div>
	<div class="offcanvas-body pt-0">
		<ul style="list-style-type: none;" class="p-0">
			<li><a class="sidebar-item" href="">HUBUNGI KAMI</a></li>
			<li><a class="sidebar-item" href="">TENTANG KAMI</a></li>
			<li><a class="sidebar-item" href="">SEMUA PRODUK</a></li>
			<li><a class="sidebar-item" href="">PUSAT BANTUAN</a></li>
		</ul>
	</div>
</div>

<nav id="navbar_top" class="d-none d-sm-block navbar navbar-expand-lg navbar-light bg-white shadow-sm">
	<div class="container">
		<a class="navbar-brand" href="#">Logo</a>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<div class="container">
				<div class="col">
					<form action="index.php?data=">
						<div class="input-group flex-nowrap">
							<input type="text" class="form-control" name="search" placeholder="Cari Produk" value="<?= $cari ?>" aria-describedby="cari">
							<button class="input-group-text" type="button" onclick="submit()" id="cari"><i class="fa fa-search"></i></button>
						</div>
					</form>
				</div>
			</div>
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<div class="navbar-nav ms-auto	">
					<a class="nav-link pt-0 pb-0" aria-current="page" href="#">
						<?php
						if (!isset($_SESSION['login'])) { ?>
							<span style="font-size: 1.5em; ">
								<a href="../login.php" style="color: black;"><i class="fas fa-user" title="Login?"></i></a>
							</span>
						<?php } else { ?>
							<span style="font-size: 1.5em; ">
								<i class="fas fa-shopping-cart"></i>
							</span>
						<?php } ?>
					</a>
				</div>
			</div>
		</div>
	</div>
</nav>