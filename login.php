<?php
session_start();

if (isset($_SESSION["login"])) {
    header("Location: Marketplace");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/login/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/login/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/login/favicon-16x16.png">
    <link rel="manifest" href="assets/img/login/site.webmanifest">

    <title>Login Dekranasda</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/shop-homepage.css" rel="stylesheet">

</head>
<title>Login</title>
<div class="d-flex align-items-center justify-content-center" style="height: 500px;">
    <form action="auth.php" method="POST">
        <div class="form-group">
            <h3>Selamat Datang Manusia</h3>
            <!-- <p class="text-center">Login Sebagai?</p> -->
        </div>
        <!-- <div class="d-flex justify-content-around">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" checked>
                <label class="form-check-label" for="flexRadioDefault1">Admin</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                <label class="form-check-label" for="flexRadioDefault2">IKM</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                <label class="form-check-label" for="flexRadioDefault2">Petugas</label>
            </div>
        </div> -->
        <div class="form-group">
            <label for="usrname">Username</label>
            <input type="text" class="form-control" id="usrname" name="username" required>
            <!-- <small id="emailHelp" class="form-text text-muted">We've never seen an ugly name before you.</small> -->
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" required>
        </div>
        <div class="form-group">
            <div class="custom-control custom-checkbox small">
                <input type="checkbox" class="custom-control-input" id="customCheck" name="remember" value="1">
                <label class="custom-control-label" for="customCheck">Remember Me</label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>
</div>

<!-- Footer -->
<!-- <footer class="py-5 bg-dark"> -->
<div class="container">
    <p class="m-0 text-center text-black">Copyright &copy; Dinas Tenaga Kerja dan Perindustrian Kab. Tala</p>
</div>
<!-- /.container -->
<!-- </footer> -->

<!-- Bootstrap core JavaScript -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>